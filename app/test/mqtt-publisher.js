const mqtt = require('mqtt');
const fs = require('fs');
const config = require('../config/config');

const caFile = fs.readFileSync('myCAFile');

const options = {
  username: config.mqttUsernameD,
  password: config.mqttPasswordD,
  rejectedUnauthorized: false,
  ca: [caFile],
};

module.exports = () => {
  // const mqttClient = mqtt.connect('mqtt://platone.eng.it:1883', options);
  // const mqttClient = mqtt.connect('mqtts://94973820-baa1-45ef-9631-25902930e34f.eon.tools:8883', options);
  const mqttClient = mqtt.connect('mqtt://127.0.0.1:1883', options);

  const topicD = config.mqttTopicD;

  // const topicD = 'platone/test2';
  mqttClient.on('connect', () => {
    console.log('connected to mqtt');
    const ddemo = {
      device: 'deviceCassandraTest2',
      timestamp: '2022-02-22T09:39:46.786923+00:00',
      // timestamp: ms.now(),
      // timestamp: new Date().getTime().toString(),
      readings: [
        {
          component: 'BUS1',
          measurand: 'voltmagnitude',
          phase: 'A',
          data: 231.768,
        },
        {
          component: 'BUS1',
          measurand: 'voltangle',
          phase: 'A',
          data: 0.798,
        },
        {
          component: 'BUS1',
          measurand: 'frequency',
          phase: 'A',
          data: 50.1752,
        },
        {
          component: 'BUS1',
          measurand: 'rocof',
          phase: 'A',
          data: 1.101,
        },
        {
          component: 'BUS1',
          measurand: 'voltmagnitude',
          phase: 'B',
          data: 230.36,
        },
        {
          component: 'BUS1',
          measurand: 'voltangle',
          phase: 'B',
          data: 0.062,
        },
        {
          component: 'BUS1',
          measurand: 'frequency',
          phase: 'B',
          data: 50.0308,
        },
        {
          component: 'BUS1',
          measurand: 'rocof',
          phase: 'B',
          data: 1.077,
        },
        {
          component: 'BUS1',
          measurand: 'voltmagnitude',
          phase: 'C',
          data: 231.628,
        },
        {
          component: 'BUS1',
          measurand: 'voltangle',
          phase: 'C',
          data: 0.014,
        },
        {
          component: 'BUS1',
          measurand: 'frequency',
          phase: 'C',
          data: 49.8804,
        },
        {
          component: 'BUS1',
          measurand: 'rocof',
          phase: 'C',
          data: 1.093,
        },
        {
          component: 'BUS1',
          measurand: 'currmagnitude',
          phase: 'A',
          data: 20.164,
        },
        {
          component: 'BUS1',
          measurand: 'currangle',
          phase: 'A',
          data: 0.824,
        },
        {
          component: 'BUS1',
          measurand: 'frequency',
          phase: 'A',
          data: 49.8384,
        },
        {
          component: 'BUS1',
          measurand: 'rocof',
          phase: 'A',
          data: 0.828,
        },
        {
          component: 'BUS1',
          measurand: 'currmagnitude',
          phase: 'B',
          data: 19.416,
        },
        {
          component: 'BUS1',
          measurand: 'currangle',
          phase: 'B',
          data: 0.044,
        },
        {
          component: 'BUS1',
          measurand: 'frequency',
          phase: 'B',
          data: 49.8136,
        },
        {
          component: 'BUS1',
          measurand: 'rocof',
          phase: 'B',
          data: 1.367,
        },
        {
          component: 'BUS1',
          measurand: 'currmagnitude',
          phase: 'C',
          data: 19.164,
        },
        {
          component: 'BUS1',
          measurand: 'currangle',
          phase: 'C',
          data: 0.075,
        },
        {
          component: 'BUS1',
          measurand: 'frequency',
          phase: 'C',
          data: 49.9952,
        },
        {
          component: 'BUS1',
          measurand: 'rocof',
          phase: 'C',
          data: 1.236,
        },
      ],
    };
    setInterval(() => {
      // ddemo.timestamp = ms.now();
      ddemo.timestamp = '2022-02-22T09:39:46.786923+00:00';
      mqttClient.publish(topicD, JSON.stringify(ddemo));
    }, 1000);
  });
  mqttClient.on('error', (err) => {
    console.log(err);
    console.log('mqtt client error');
  });
  mqttClient.on('close', () => {
    console.log('mqtt client disconnected');
  });
};
