const web3 = require('../config/utils').init;

const pmuMeterReadingLib = require('../libs/pmuMeterReading');

module.exports = async (req, res) => {
  const params = {};

  const { dateFrom } = req.query;
  console.log(`dateFrom  ${dateFrom}`);
  const { dateTo } = req.query;
  console.log(`dateTo  ${dateTo}`);
  const { owner } = req.query;
  console.log(`owner  ${owner}`);

  params.datetime = { $gte: new Date(dateFrom), $lte: new Date(dateTo) };
  params.owner = owner;
  const meterReadings = await pmuMeterReadingLib.get(params);

  if (meterReadings.length > 0) {
    const hash = web3.utils.soliditySha3(meterReadings);
    console.log(`HASH ----> ${hash}`);
    res.json({ meterReadings, hash });
  } else {
    console.log('MeterRaeadings is empty');
  }
};
