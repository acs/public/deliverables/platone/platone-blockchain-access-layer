const abiDecoder = require('abi-decoder');
const web3 = require('../config/utils').init;

const contractJson = require('../build/contracts/PlatoneCertification.json');

abiDecoder.addABI(contractJson.abi);

web3.eth
  .getTransaction(
    '0xd96769b7e1df601556f096e21c56af76b11177ba65dd4c30406672e4888c403b',
  )
  .then((transaction) => {
    console.log(transaction);
    const decodedData = abiDecoder.decodeMethod(transaction.input);
    console.log(decodedData);
  });
