const abiDecoder = require('abi-decoder');
const web3 = require('../config/utils').init;

const contractJson = require('../build/contracts/SubscriptionContract.json');

abiDecoder.addABI(contractJson.abi);

web3.eth
  .getTransaction(
    "0x20ac74080ef6976d76fc5de7af8f276ba4b190f1ec3849a6ffc4769876e7e75f",
  )
  .then((transaction) => {
    console.log(transaction);
    const decodedData = abiDecoder.decodeMethod(transaction.input);
    console.log(decodedData);
  });
