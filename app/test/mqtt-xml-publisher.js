const fs = require('fs-extra');
const path = require('path');
const mqtt = require('mqtt');
const config = require('../config/config');

const options = {
  username: config.mqttUsernameG,
  password: config.mqttPasswordG,
  rejectedUnauthorized: false,
};

const filePath = path.join(__dirname, config.xmlFilePath);

const mqttClient = mqtt.connect(config.mqttHost, options);
const topicG = config.mqttTopicG;

mqttClient.on('connect', () => {
  mqttClient.subscribe(topicG, (err) => {
    if (!err) {
      fs.readFile(filePath, { encoding: 'utf-8' }, (_error, xmlData) => {
        console.log(_error);
        console.log(xmlData);
        mqttClient.publish(topicG, xmlData);
      });
    }
  });
});
