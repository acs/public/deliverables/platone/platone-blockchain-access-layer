const mqtt = require('mqtt');
const fs = require('fs');
const config = require('../config/config');

const caFile = fs.readFileSync('myCAFile');

const options = {
  username: config.mqttUsernameAdmin,
  // password: config.mqttPasswordAdminGerman,
  password: config.mqttPasswordAdmin,
  rejectedUnauthorized: false,
  ca: [caFile],
};

const mqttClient = mqtt.connect('mqtt://platone.eng.it:1883', options);
// const mqttClient = mqtt.connect('mqtts://94973820-baa1-45ef-9631-25902930e34f.eon.tools:8883', options);
// const mqttClient = mqtt.connect('mqtt://127.0.0.1:1883', options);

const topicD = config.mqttTopicD;
// const topicD = 'test2'
// const topicDsotpGreek = config.mqttTopicDsotpGreek;

mqttClient.on('connect', () => {
  console.log('connected');
  mqttClient.subscribe(topicD, (err) => {
    if (err) {
      console.log(err);
    } else {
      mqttClient.on('message', async (topic, message) => {
        const jsonData = JSON.parse(message);
        console.log(jsonData);
      });
    }
  });
});
