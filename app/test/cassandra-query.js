const cassandra = require('cassandra-driver');
const web3 = require('../config/utils').init;

const config = require('../config/config');

module.exports = (req, res) => {
  const { Mapper } = cassandra.mapping;

  const cassandraClient = new cassandra.Client({
    contactPoints: [config.cassandraContactPointsIP],
    localDataCenter: config.localDataCenterCassandra,
    keyspace: config.keyspaceDdemo,
  });

  const mapper = new Mapper(cassandraClient, {
    models: {
      MeterReading: {},
    },
  });
  const meterReadingMapper = mapper.forModel('MeterReading');

  cassandraClient.connect().then(async () => {
    const query = config.queryGetMeterReadingsByOwnerAndTimestamp;
    const { dateFrom } = req.query;
    const { dateTo } = req.query;
    const { owner } = req.query;

    meterReadingMapper.getMeterReadings = meterReadingMapper.mapWithQuery(query, (meterReading) => [meterReading.owner, meterReading.dateFrom, meterReading.dateTo]);

    const meterReadings = await meterReadingMapper.getMeterReadings({ owner, dateFrom, dateTo });

    console.log(meterReadings);

    const hash = web3.utils.soliditySha3(meterReadings);
    console.log(`HASH ----> ${hash}`);
    res.json({ meterReadings, hash });
  });
};
