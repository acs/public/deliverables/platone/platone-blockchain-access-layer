const mqtt = require('mqtt');

const xml2js = require('xml2js');
const meterReadingLib = require('../libs/meterReading');
const MeterReadingModel = require('../models/dataModels/MeterReadingModel');
const userLib = require('../libs/user');

const { stripPrefix } = xml2js.processors;
const config = require('../config/config');
const logger = require('../logger');

const options = {
  username: config.mqttUsernameAdmin,
  password: config.mqttPasswordAdmin,
  rejectedUnauthorized: false,
};

const parser = new xml2js.Parser({
  mergeAttrs: true,
  explicitRoot: false,
  trim: true,
  explicitArray: false,
  tagNameProcessors: [stripPrefix],
  attrNameProcessors: [stripPrefix],
});

class MqttHandler {
  constructor(host) {
    this.mqttClient = null;
    this.host = host;
  }

  connect() {
    this.mqttClient = mqtt.connect(this.host, options);
    // Mqtt error calback
    this.mqttClient.on('error', (err) => {
      logger.error(err);
      logger.info('ERRORE MQTT');
      this.mqttClient.end();
    });

    // Connection callback
    this.mqttClient.on('connect', () => {
      logger.info('mqtt client connected');
      // mqtt subscriptions
    });

    const topicG = config.mqttTopicG;

    this.mqttClient.subscribe([topicG]);

    this.mqttClient.on('message', (topic, message) => {
      if (topic === topicG) {
        const data = message;
        parser.parseString(data, async (err, res) => {
          if (err) logger.error(err);

          const owner = await userLib.getOwnerByTopic(topic, 'producer');

          logger.info(`ONWER-> ${owner}`);
          logger.info(JSON.stringify(res));

          for (let i = 0; i < res.MeterReading.length; i += 1) {
            const meterReading = new MeterReadingModel({
              Meter: data.Meter,
              Readings: data.Readings,
              UsagePoint: data.UsagePoint,
            });

            logger.info(` ${topic} METER READING Meter>>> ${JSON.stringify(meterReading.Meter)}`);
            logger.info(` ${topic} METER READING Readings>>> ${JSON.stringify(meterReading.Readings)}`);
            logger.info(` ${topic} METER READING UsagePoint>>> ${JSON.stringify(meterReading.UsagePoint)}`);

            const meterReadingObj = {
              owner,
              datetime: new Date(),
              Meter: meterReading.Meter,
              Readings: meterReading.Readings,
              UsagePoint: meterReading.UsagePoint,
            };

            meterReadingLib.save(meterReadingObj);

            const newMeterReading = {
              Meter: data.Meter,
              Readings: data.Readings,
              UsagePoint: data.UsagePoint,
            };
            this.mqttClient.publish(config.mqttTopicDsotpGreek, JSON.stringify(newMeterReading));
          }
        });
      }
    });

    this.mqttClient.on('close', () => {
      logger.info('mqtt client disconnected');
    });
  }
}

module.exports = MqttHandler;
