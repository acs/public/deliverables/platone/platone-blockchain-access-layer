/* eslint-disable no-console */
/* eslint-disable no-use-before-define */
/* eslint-disable max-len */
const schedule = require('node-schedule');
const cassandra = require('cassandra-driver');
const moment = require('moment');

const { Mapper } = cassandra.mapping;
const config = require('../config/config');
const logger = require('../logger');
const energyReadingLib = require('../libs/certification');
const producerLib = require('../libs/producer');
const web3 = require('../config/utils').init;
const readingContract = require('../services/contracts').platoneCertification(web3);
const ContractUtil = require('../services/contracts').utils();

const readingContractAddress = readingContract.getAddress();

const cassandraClient = new cassandra.Client({
  contactPoints: [config.cassandraContactPointsIP],
  localDataCenter: config.localDataCenterCassandra,
  keyspace: config.keyspaceDdemo,
});

exports.startScheduling = function () {
  const j1 = new schedule.Job(() => {
    logger.info('Read from CASSANDRA for data certification');
    getEnergyReadings();
  });
  const rule = new schedule.RecurrenceRule();
  rule.minute = new schedule.Range(0, 59, 3);
  j1.schedule(rule);
};

const mapper = new Mapper(cassandraClient, {
  models: {
    MeterReading: {
    },
  },
});

const meterReadingMapper = mapper.forModel('MeterReading');
function getEnergyReadings() {
  producerLib.getAll().then((producers) => {
    for (let i = 0; i < producers.length; i += 1) {
      const owner = producers[i].wallet;

      cassandraClient.connect().then(async () => {
        const datetimeNow = moment();
        const query = config.queryGetMeterReadingsByOwnerAndTimestamp;
        const dateFrom = moment(datetimeNow).subtract(3, 'minutes').utc().format();
        const dateTo = datetimeNow.utc().format();

        logger.info('Read from CASSANDRA for data certification');

        logger.info(`owner ${owner}`);
        logger.info(`dateFrom ${dateFrom}`);
        logger.info(`dateTo ${dateTo}`);

        meterReadingMapper.getMeterReadings = meterReadingMapper.mapWithQuery(query, (meterReading) => [meterReading.owner, meterReading.dateFrom, meterReading.dateTo]);

        const meterReadings = await meterReadingMapper.getMeterReadings({ owner, dateFrom, dateTo });

        logger.info(`meterReadings ${meterReadings.length}`);

        if (meterReadings.length > 0) {
          const hash = web3.utils.soliditySha3(meterReadings);
          await ContractUtil.unlock(owner);
          readingContract.connect(readingContractAddress).then(() => {
            readingContract.energySnapshot(owner, Date.now(), hash).then((data) => {
              const certification = {
                owner,
                hashedData: hash,
                transactionHash: data.tx,
                dataset: { dateFrom, dateTo },
              };
              logger.info(`CERTIFICATION>>>${JSON.stringify(certification)}`);
              energyReadingLib.save(certification);
            }).catch((err) => {
              logger.error(err);
            });
          });
        }
      }).catch(async (err2) => {
        logger.error('There was an error when connecting', err2);
        await cassandraClient.shutdown();
        throw err2;
      });
    }
  }).catch((err) => {
    logger.info(err);
  });
}
