const schedule = require('node-schedule');
const logger = require('../logger');
const scdService = require('../services/scd');
const config = require('../config/config');
// const testPublisher = require('../test/mqtt-publisher');

exports.startScheduling = () => {
  const j1 = new schedule.Job(() => {
    logger.info('Read from SCD for data certification Ddemo');
    scdService.certification(config.keyspaceDdemo);
  });
  const j2 = new schedule.Job(() => {
    logger.info('Read from SCD for data certification Gdemo');
    scdService.certification(config.keyspaceGdemo);
  });
  const j3 = new schedule.Job(() => {
    logger.info('Read from SCD for data publishing');
    scdService.publishing(config.keyspaceDdemo);
  });
  const j4 = new schedule.Job(() => {
    logger.info('Publish Fake data on MQTT');
    // testPublisher();
  });
  const rule = new schedule.RecurrenceRule();
  rule.minute = new schedule.Range(0, 59, 1);

  const rule2 = new schedule.RecurrenceRule();
  rule2.minute = new schedule.Range(0, 59, 1);

  j1.schedule(rule);
  // j2.schedule(rule);
  // j3.schedule(rule2);
  // j4.schedule(rule2);
};
