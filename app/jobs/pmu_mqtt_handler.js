const mqtt = require("mqtt");
const fs = require("fs");
const path = require("path");

const userLib = require("../libs/user");
const pmuMeterReadingLib = require("../libs/pmuMeterReading");
const PMUMeterReadingModel = require("../models/dataModels/PMUMeterReadingModel");
const config = require("../config/config");
const logger = require("../logger");

const options = {
  username: config.mqttUsernameAdmin,
  password: config.mqttPasswordAdmin,
  rejectedUnauthorized: false,
  cafile: fs.readFileSync(
    path.resolve(__dirname, "./../mosquitto/config/certs/fullchain.pem"),
  ),
};

class MqttHandler {
  constructor(host) {
    this.mqttClient = null;
    this.host = host;
  }

  connect() {
    this.mqttClient = mqtt.connect(this.host, options);
    // Mqtt error calback
    this.mqttClient.on("error", (err) => {
      logger.error(err);
      logger.info("ERRORE MQTT");
      this.mqttClient.end();
    });

    // Connection callback
    this.mqttClient.on("connect", () => {
      logger.info("mqtt client connected");
      // mqtt subscriptions
    });

    const topicD = config.mqttTopicD;
    const topicD2 = "platone/test2";

    this.mqttClient.subscribe([topicD]);
    this.mqttClient.subscribe([topicD2]);

    this.mqttClient.on("message", async (topic, message) => {
      const entryMessage = message;
      const jsonData = JSON.parse(entryMessage);

      const datePart1 = jsonData.timestamp.split(".")[0];
      const dateSeconds = new Date(datePart1).getTime() / 1000;
      const datePart2 = jsonData.timestamp.split(".")[1];
      const microseconds = datePart2.split("+")[0];
      const timestampF = dateSeconds + microseconds;

      const meterReading = new PMUMeterReadingModel({
        device: jsonData.device,
        timestamp: parseInt(timestampF, 10),
        readings: jsonData.readings,
      });

      logger.info(
        ` ${topic} MU MEPTER READING from device>>> ${JSON.stringify(
          meterReading.device,
        )}`,
      );
      logger.info(
        ` ${topic} PMU METER READING with timestamp>>> ${JSON.stringify(
          meterReading.timestamp,
        )}`,
      );

      const owner = await userLib.getOwnerByTopic(topic, "producer");
      logger.info(` owner>>> ${JSON.stringify(owner)}`);

      const meterReadingObj = {
        owner,
        datetime: new Date(),
        device: meterReading.device,
        timestamp: meterReading.timestamp,
        readings: meterReading.readings,
      };

      pmuMeterReadingLib.save(meterReadingObj);

      // String Timestamp for DSOTP
      const newMeterReading = new PMUMeterReadingModel({
        device: jsonData.device,
        timestamp: jsonData.timestamp.toString(),
        readings: jsonData.readings,
      });
      this.mqttClient.publish(
        config.mqttTopicDsotp,
        JSON.stringify(newMeterReading),
      );
    });

    this.mqttClient.on("close", () => {
      logger.info("mqtt client disconnected");
    });
  }
}

module.exports = MqttHandler;
