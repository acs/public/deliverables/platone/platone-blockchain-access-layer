const mqtt = require('mqtt');
const cassandra = require('cassandra-driver');

const { Mapper } = cassandra.mapping;
const { Uuid } = cassandra.types;

const xml2js = require('xml2js');
const MeterReading = require('../models/MeterReadingModel');
const userLib = require('../libs/user');

const { stripPrefix } = xml2js.processors;
const config = require('../config/config');
const logger = require('../logger');

const options = {
  username: config.mqttUsernameAdmin,
  password: config.mqttPasswordAdmin,
  rejectedUnauthorized: false,
};

const parser = new xml2js.Parser({
  mergeAttrs: true,
  explicitRoot: false,
  trim: true,
  explicitArray: false,
  tagNameProcessors: [stripPrefix],
  attrNameProcessors: [stripPrefix],
});

class MqttHandler {
  constructor(host) {
    this.mqttClient = null;
    this.host = host;
  }

  connect() {
    this.mqttClient = mqtt.connect(this.host, options);
    const cassandraClient = new cassandra.Client({
      contactPoints: [config.cassandraContactPointsIP],
      localDataCenter: config.localDataCenterCassandra,
      keyspace: config.keyspaceGdemo,
    });
    // Mqtt error calback
    this.mqttClient.on('error', (err) => {
      logger.error(err);
      logger.info('ERRORE MQTT');
      this.mqttClient.end();
    });

    // Connection callback
    this.mqttClient.on('connect', () => {
      logger.info('mqtt client connected');
      // mqtt subscriptions
    });

    const topicG = config.mqttTopicG;

    this.mqttClient.subscribe([topicG]);

    this.mqttClient.on('message', (topic, message) => {
      if (topic === topicG) {
        const data = message;
        const meterReadings = [];
        parser.parseString(data, async (err, res) => {
          if (err) logger.error(err);
          const mapper = new Mapper(cassandraClient, {
            models: {
              MeterReading: {
                tables: ['meterreading'],
              },
            },
          });
          const MeterReadingMapper = mapper.forModel('MeterReading');
          const owner = await userLib.getOwnerByTopic(topic, 'producer');
          logger.info(`ONWER-> ${owner}`);
          logger.info(JSON.stringify(res));

          for (let i = 0; i < res.MeterReading.length; i += 1) {
            const meterReading = new MeterReading({
              Meter: res.MeterReading[i].Meter,
              Readings: res.MeterReading[i].Readings,
              UsagePoint: res.MeterReading[i].UsagePoint,
            });

            meterReadings.push(meterReading);

            cassandraClient.connect().then(() => {
              MeterReadingMapper.insert({
                id: Uuid.random(),
                meter: meterReading.Meter,
                readings: meterReading.Readings,
                usagepoint: meterReading.UsagePoint,
                owner,
                datetime: new Date(),
              });
            }).catch((err2) => {
              logger.error('There was an error when connecting', err2);
              return cassandraClient.shutdown().then(() => { throw err2; });
            });
            const newMeterReading = new MeterReading({
              Meter: res.MeterReading[i].Meter,
              Readings: res.MeterReading[i].Readings,
              UsagePoint: res.MeterReading[i].UsagePoint,
            });
            this.mqttClient.publish(config.mqttTopicDsotpGreek, JSON.stringify(newMeterReading));
          }
        });
      }
    });

    this.mqttClient.on('close', () => {
      logger.info('mqtt client disconnected');
    });
  }
}

module.exports = MqttHandler;
