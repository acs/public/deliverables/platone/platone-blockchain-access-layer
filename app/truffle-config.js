module.exports = {
  networks: {
    local: {
      host: "geth_node1",
      port: 8501,
      network_id: "32414",
      from: "0xef46e8cafe1b76c92c13824cea5e2e7b1301284e",
      gas: 1000000,
    },
    psm: {
      host: "161.27.206.144",
      port: 8501,
      network_id: "32414",
      from: "0xef46e8cafe1b76c92c13824cea5e2e7b1301284e",
      gas: 5141445,
    },
  },
  compilers: {
    solc: {
      version: "0.8.4",
    },
  },
};
