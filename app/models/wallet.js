const mongoose = require('mongoose');

const { Schema } = mongoose;

const walletSchema = Schema({
  id_wallet: { type: String, unique: true },
  balance: { type: Number },
});

const Wallet = mongoose.model('Wallet', walletSchema);
module.exports = Wallet;
