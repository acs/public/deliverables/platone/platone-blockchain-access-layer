const { ObjectModel } = require('objectmodel');

class UsagePointModel extends ObjectModel({ mRID: String }) {
  constructor({ mRID }) {
    super({ mRID });
  }
}

module.exports = { UsagePointModel };
