const { ObjectModel } = require('objectmodel');
const { NameTypeModel } = require('./NameTypeModel');

class NameModel extends ObjectModel({ name: String, NameType: NameTypeModel }) {
  constructor({ name, NameType }) {
    super({ name, NameType });
  }
}

module.exports = { NameModel };
