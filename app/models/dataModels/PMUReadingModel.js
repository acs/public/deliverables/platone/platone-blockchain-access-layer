const { ObjectModel } = require('objectmodel');

class PMUReadingModel extends ObjectModel({
  component: String, measurand: String, phase: String, data: Number,
}) {
  constructor({
    component, measurand, phase, data,
  }) {
    super({
      component, measurand, phase, data,
    });
  }
}

module.exports = { PMUReadingModel };
