const { ObjectModel } = require('objectmodel');

class ReadingTypeModel extends ObjectModel({
  ref: String,
  macroPeriod: [String],
  aggregate: [String],
  measuringPeriod: [String],
  accumulation: [String],
  flowDirection: [String],
  commodity: [String],
  measurementKind: [String],
  interharmonicNumerator: [String],
  interharmoniDenominator: [String],
  argumentNumerator: [String],
  argumentDenominator: [String],
  tou: [String],
  cpp: [String],
  consumptionTier: [String],
  phases: [String],
  multiplier: [String],
  unit: [String],
  currency: [String],
}) {
  constructor({ ref }) {
    super({
      ref,
      macroPeriod: ref.split('.')[0],
      aggregate: ref.split('.')[1],
      measuringPeriod: ref.split('.')[2],
      accumulation: ref.split('.')[3],
      flowDirection: ref.split('.')[4],
      commodity: ref.split('.')[5],
      measurementKind: ref.split('.')[6],
      interharmonicNumerator: ref.split('.')[7],
      interharmoniDenominator: ref.split('.')[8],
      argumentNumerator: ref.split('.')[9],
      argumentDenominator: ref.split('.')[10],
      tou: ref.split('.')[11],
      cpp: ref.split('.')[12],
      consumptionTier: ref.split('.')[13],
      phases: ref.split('.')[14],
      multiplier: ref.split('.')[15],
      unit: ref.split('.')[16],
      currency: ref.split('.')[17],
    });
  }
}

module.exports = { ReadingTypeModel };
