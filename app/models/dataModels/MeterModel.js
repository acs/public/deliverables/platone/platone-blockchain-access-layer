const { ObjectModel } = require('objectmodel');
const { NameModel } = require('./NameModel');

class MeterModel extends ObjectModel({ Names: NameModel }) {
  constructor({ Names }) {
    super({ Names });
  }
}

module.exports = { MeterModel };
