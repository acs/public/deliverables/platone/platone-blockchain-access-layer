const { ObjectModel, ArrayModel } = require('objectmodel');
const { PMUReadingModel } = require('./PMUReadingModel');

class PMUMeterReadingModel extends ObjectModel({ device: String, timestamp: [String, Number, Date], readings: ArrayModel(PMUReadingModel) }) {
  constructor({ device, timestamp, readings }) {
    super({ device, timestamp, readings });
  }
}

module.exports = PMUMeterReadingModel;
