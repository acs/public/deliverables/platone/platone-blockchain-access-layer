const { ObjectModel } = require('objectmodel');
const { NameTypeAuthorityModel } = require('./NameTypeAuthorityModel');

class NameTypeModel extends ObjectModel({ name: String, NameTypeAuthority: NameTypeAuthorityModel }) {
  constructor({ name, NameTypeAuthority }) {
    super({ name, NameTypeAuthority });
  }
}

module.exports = { NameTypeModel };
