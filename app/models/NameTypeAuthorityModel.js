const { ObjectModel } = require('objectmodel');

class NameTypeAuthorityModel extends ObjectModel({ name: String }) {
  constructor({ name }) {
    super({ name });
  }
}

module.exports = { NameTypeAuthorityModel };
