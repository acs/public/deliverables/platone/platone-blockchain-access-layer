const { ObjectModel } = require('objectmodel');

class Interharmonic extends ObjectModel({ numerator: Number, denominator: Number }) {
  constructor({ numerator, denominator }) {
    super({ numerator, denominator });
  }
}

module.exports = { Interharmonic };
