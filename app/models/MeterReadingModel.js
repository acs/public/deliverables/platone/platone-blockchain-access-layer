const { ObjectModel, ArrayModel } = require('objectmodel');
const { MeterModel } = require('./MeterModel');
const { ReadingModel } = require('./ReadingModel');
const { UsagePointModel } = require('./UsagePointModel');

function isArray(val) {
  if (Array.isArray(val)) {
    return val;
  }
  const res = [];
  res.push(val);
  return res;
}

class MeterReadingModel extends ObjectModel({ Meter: MeterModel, Readings: [ReadingModel, ArrayModel(ReadingModel)], UsagePoint: UsagePointModel }) {
  constructor({ Meter, Readings, UsagePoint }) {
    super({ Meter, Readings: isArray(Readings), UsagePoint });
  }
}

module.exports = MeterReadingModel;
