const mongoose = require('mongoose');

const { Schema } = mongoose;

const pmuMeterReadingSchema = Schema({
  owner: { type: String },
  datetime: { type: Date },
  device: { type: String },
  timestamp: { type: String },
  readings: [{
    component: { type: String },
    measurand: { type: String },
    phase: { type: String },
    data: { type: Number },
  }],
});

pmuMeterReadingSchema.index({ owner: 1, datetime: 1 });
const PmuMeterReading = mongoose.model('PmuMeterReading', pmuMeterReadingSchema);
module.exports = PmuMeterReading;
