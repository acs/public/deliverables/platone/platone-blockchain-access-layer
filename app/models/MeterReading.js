const mongoose = require('mongoose');

const { Schema } = mongoose;

const meterReadingSchema = Schema({
  meter: {
    names: {
      name: { type: String },
      nameType: {
        name: { type: String },
        nameTypeAuthority: {
          name: { type: String },
        },
      },
    },
  },
  readings: [{
    timestamp: { type: Date },
    value: { type: Number },
    readingType: { type: String },
  }],
  usagePoint: {
    mRID: { type: Number },
  },
});

const MeterReading = mongoose.model('MeterReading', meterReadingSchema);
module.exports = MeterReading;
