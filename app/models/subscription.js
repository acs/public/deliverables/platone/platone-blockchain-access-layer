const mongoose = require('mongoose');

const { Schema } = mongoose;

const subscriptionSchema = Schema({
  id_consumer: { type: String, required: true },
  topic: { type: String, required: true },
  status: { type: String, enum: ['REQUESTED', 'ACCEPTED', 'REFUSED'], default: 'REQUESTED' },
  creation_date: { type: Date },
  modification_date: { type: Date },
});

const Subscription = mongoose.model('Subscription', subscriptionSchema);
module.exports = Subscription;
