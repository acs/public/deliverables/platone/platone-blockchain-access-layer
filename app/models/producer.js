const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const producerSchema = mongoose.Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true, select: false },
  topic: { type: String },
  wallet: { type: String, unique: true },
  account: {},
});

producerSchema.methods.generateHash = function generateHash(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

producerSchema.methods.validPassword = function validPassword(password) {
  return bcrypt.compareSync(password, this.password);
};

module.exports = mongoose.model('Producer', producerSchema);
