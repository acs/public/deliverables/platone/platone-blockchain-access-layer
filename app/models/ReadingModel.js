const { ObjectModel } = require('objectmodel');
const { ReadingTypeModel } = require('./ReadingTypeModel');

class ReadingModel extends ObjectModel({ timeStamp: [String, Date], value: String, ReadingType: ReadingTypeModel }) {
  constructor({ timeStamp, value, ReadingType }) {
    super({ timeStamp: new Date(timeStamp), value, ReadingType });
    // super({ timeStamp, value, ReadingType });
  }
}

module.exports = { ReadingModel };
