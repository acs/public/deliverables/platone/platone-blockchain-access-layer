const mongoose = require('mongoose');

const { Schema } = mongoose;

const certificationSchema = Schema({
  hashedData: { type: String },
  owner: { type: String },
  createdAt: { type: Date, default: Date.now() },
  transactionHash: { type: String },
  dataset: { type: Object },
});

const Certification = mongoose.model('Certification', certificationSchema);
module.exports = Certification;
