/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
const mongoose = require('mongoose');
const ContractUtil = require('../services/contracts').utils();

const mongo = process.env.DATABASE_URL || 'localhost:27017';
// const mongo = process.env.DATABASE_URL || '161.27.206.144:27017';
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.connect(`mongodb://${mongo}/bal`);
const User = require('../models/user');

const users = [
  {
    username: 'ddemo',
    role: 'producer',
  },
  {
    username: 'gdemo',
    role: 'producer',
  },
];

const promises = [];

async function main() {
  for (const i in users) {
    const userObj = users[i];
    const account = ContractUtil.newWallet();
    console.log(account);
    // ContractUtil.unlock(address);
    const newUser = new User();
    newUser.topic = 'platone/'.concat(userObj.username);
    newUser.username = userObj.username;
    newUser.password = newUser.generateHash('password');
    newUser.role = userObj.role;
    newUser.wallet = account.address;
    newUser.account = account;
    promises.push(newUser.save());
  }

  Promise.all(promises).then((result) => {
    console.log(result);
    mongoose.connection.close();
  }).catch((err) => {
    console.log(err);
    mongoose.connection.close();
  });
}

main();
