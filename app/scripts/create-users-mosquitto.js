/* eslint-disable no-await-in-loop */
const mongoose = require('mongoose');
const { MosquittoDynsec } = require('mosquitto-dynsec');
const config = require('../config/config');

const ContractUtil = require('../services/contracts').utils();

const mongo = process.env.DATABASE_URL || 'localhost:27017';
// const mongo = process.env.DATABASE_URL || '161.27.206.144:27017';
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.connect(`mongodb://${mongo}/bal`);
const User = require('../models/user');

const dynsec = new MosquittoDynsec();

const users = [
  {
    username: 'producer1',
    role: 'producer',
  },
  {
    username: 'producer2',
    role: 'producer',
  },
  {
    username: 'producer3',
    role: 'producer',
  },
  {
    username: 'consumer1',
    role: 'consumer',
  },
  {
    username: 'consumer2',
    role: 'consumer',
  },
  {
    username: 'consumer3',
    role: 'consumer',
  },
];

const promises = [];

async function main() {
  try {
    await dynsec.connect({
      username: config.mosquittoAdmin,
      password: config.mosquittoPasswordAdmin,
    });
  } catch (e) {
    console.error('Error:', e);
  }

  for (let i = 0; i < users.length; i += 1) {
    const userObj = users[i];
    console.log(`userObj>>> ${JSON.stringify(userObj)}`);
    const account = ContractUtil.newWallet();
    console.log(account);
    const newUser = new User();
    newUser.role = userObj.role;
    console.log(`ROLE>>> ${JSON.stringify(userObj.role)}`);
    if (userObj.role === 'producer') {
      newUser.topic = 'platone/'.concat(userObj.username);
      promises.push(await dynsec.createRole(`write${userObj.username}`));
      promises.push(await dynsec.addRoleACL({
        rolename: `write${userObj.username}`,
        acltype: 'publishClientSend',
        topic: newUser.topic,
        allow: true,
      }));
      promises.push(await dynsec.createClient({ username: userObj.username, password: newUser.generateHash('password') }));
      promises.push(await dynsec.addClientRole(userObj.username, `write${userObj.username}`));
    } else {
      promises.push(await dynsec.createClient({ username: userObj.username, password: newUser.generateHash('password') }));
    }
    newUser.username = userObj.username;
    newUser.password = newUser.generateHash('password');
    newUser.wallet = account.address;
    newUser.account = account;

    promises.push(await newUser.save());
  }

  Promise.all(promises).then((result) => {
    console.log(result);
    mongoose.connection.close();
  }).catch((err) => {
    console.log(err);
    mongoose.connection.close();
  });
}

main();
