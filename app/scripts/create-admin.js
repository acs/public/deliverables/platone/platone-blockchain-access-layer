const mongoose = require('mongoose');

const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${mongo}/bal`);
const User = require('../models/user');

const newUser = new User();

newUser.username = 'admin';
newUser.password = newUser.generateHash('admin');
newUser.role = 'admin';

newUser.save().then((result) => {
  console.log(result);
  mongoose.connection.close();
}).catch((err) => {
  console.log(err);
  mongoose.connection.close();
});
