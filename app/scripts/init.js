/* eslint-disable no-await-in-loop */
/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
const mongoose = require('mongoose');
const { MosquittoDynsec } = require('mosquitto-dynsec');
const config = require('../config/config');
const ContractUtil = require('../services/contracts').utils();

// const mongo = process.env.DATABASE_URL || 'localhost:27017';
const mongo = process.env.DATABASE_URL || 'localhost:27017';
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.connect(`mongodb://${mongo}/bal`);
const User = require('../models/user');

const dynsec = new MosquittoDynsec();

const usernames = [
  'admin',
];
const promises = [];

async function main() {
  try {
    await dynsec.connect({
      username: config.mosquittoAdmin,
      password: config.mosquittoPasswordAdmin,
    });
  } catch (e) {
    console.error('Error:', e);
  }
  for (const i in usernames) {
    const account = ContractUtil.newWallet();
    console.log(account);
    const newUser = new User();
    newUser.topic = 'platone/'.concat(usernames[i]);
    newUser.username = usernames[i];
    newUser.password = newUser.generateHash(usernames[i]);
    newUser.wallet = account.address;
    newUser.account = account;
    promises.push(await dynsec.createClient({ username: usernames[i], password: newUser.generateHash(usernames[i]) }));
    promises.push(newUser.save());
  }

  Promise.all(promises).then((result) => {
    console.log(result);
    mongoose.connection.close();
  }).catch((err) => {
    console.log(err);
    mongoose.connection.close();
  });
}

main();
