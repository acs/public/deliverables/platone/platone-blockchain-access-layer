const libs = require('../../libs/subscription');

module.exports = (req, res) => {
  libs.getAll()
    .then((subscriptions) => res.json(subscriptions))
    .catch((err) => res.status(500).json(err));
};
