const libs = require('../../libs/subscription');

module.exports = (req, res) => {
  const { topic } = req.params;
  libs.get({ topic })
    .then((data) => res.json(data))
    .catch((error) => res.status(500).json(error));
};
