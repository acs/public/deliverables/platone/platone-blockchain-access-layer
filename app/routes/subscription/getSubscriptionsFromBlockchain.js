const web3 = require("../../config/utils").init;
const contract = require("../../services/contracts").subscriptionCertification(web3);

module.exports = async (req, res) => {
  const { topic } = req.params;
  const contractAddress = contract.getAddress();
  console.log(`contractAddress>> ${contractAddress}`);

  console.log(topic);
  contract
    .connect(contractAddress)
    .then(async () => {
      console.log(`subscriptionContract CONNECT>>`);
      const myContract = contract.getContract().contract;
      console.log(`myContract>>${myContract}`);
      myContract.methods
        .getAcceptedSubscriptions(topic)
        .then((result) => {
          res.json(result);
          console.log(result);
        })
        .catch((err) => res.json(err));
    })
    .catch((err) => res.json(err));
};
