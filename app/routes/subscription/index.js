const subscription = require('express').Router();

const save = require('./save');
const getAll = require('./getAll');
const getByTopic = require('./getByTopic');
const getSubscriptionsFromBlockchain = require("./getSubscriptionsFromBlockchain");

subscription.post('/', save);
subscription.get('/', getAll);
subscription.get('/:topic([^/]+/[^/]+)', getByTopic);
subscription.get("/data/:topic([^/]+/[^/]+)", getSubscriptionsFromBlockchain);

module.exports = subscription;
