const { MosquittoDynsec } = require("mosquitto-dynsec");
const logger = require("../../logger");
const config = require("../../config/config");
const libs = require("../../libs/subscription");
const userLib = require("../../libs/user");
const web3 = require("../../config/utils").init;
const subscriptionContract = require("../../services/contracts").subscriptionCertification(web3);

module.exports = async (req, res) => {
  const subscriptionContractAddress = subscriptionContract.getAddress();
  logger.info(`subscriptionContractAddress>> ${subscriptionContractAddress}`);

  if (req.body.status === "ACCEPTED") {
    const dynsec = new MosquittoDynsec();
    try {
      await dynsec.connect({
        username: config.mosquittoAdmin,
        password: config.mosquittoPasswordAdmin,
      });
    } catch (e) {
      console.error("Error:", e);
    }
    await dynsec.createRole(`read${req.body.id_consumer}`);
    await dynsec.addRoleACL({
      rolename: `read${req.body.id_consumer}`,
      acltype: "publishClientReceive",
      topic: req.body.topic,
      allow: true,
    });
    await dynsec.addClientRole(
      req.body.id_consumer,
      `read${req.body.id_consumer}`,
    );

    const producerAccount = await userLib.getAccountByUsername(
      req.body.id_producer,
    );
    logger.info(`producerAccount ${JSON.stringify(producerAccount)}`);
    const consumerAccount = await userLib.getAccountByUsername(
      req.body.id_consumer,
    );
    logger.info(`consumerAccount ${JSON.stringify(consumerAccount)}`);
    const { topic } = req.body;
    const creationDate = Date.now();

    subscriptionContract
      .connect(subscriptionContractAddress)
      .then(async () => {
        logger.info(`subscriptionContract CONNECT>>`);
        const myContract = subscriptionContract.getContract().contract;
        const tx = {
          // this could be provider.addresses[0] if it exists
          from: producerAccount.address,
          // target address, this could be a smart contract address
          to: subscriptionContractAddress,
          gas: 26200,
          // this encodes the ABI of the method and the arguements
          data: myContract.methods
            .createSubscription(
              producerAccount.address,
              consumerAccount.address,
              topic,
              creationDate,
            )
            .encodeABI(),
        };
        logger.info(`TRANSACTION ${tx}`);
        logger.info(
          `PRODUCER ACCOUNT PRIVATE KEY ${producerAccount.privateKey}`,
        );

        web3.eth.accounts
          .signTransaction(tx, producerAccount.privateKey)
          .then((signedTx) => {
            const sentTx = web3.eth.sendSignedTransaction(
              signedTx.raw || signedTx.rawTransaction,
            );
            sentTx.on("receipt", (receipt) => {
              logger.info(`SUBSCRIPTION>>>${JSON.stringify(receipt)}`);
              libs
                .save(req.body)
                .then((data) => res.json(data))
                .catch((err) => res.status(500).json(err));
            });
            sentTx.on("error", (err) => {
              logger.info(`SUBSCRIPTION 2>>>${JSON.stringify(err)}`);
              libs
                .save(req.body)
                .then((data) => res.json(data))
                .catch((error) => res.status(500).json(error));
            });
          })
          .catch((err) => {
            logger.error(err);
          });
      })
      .catch((err) => {
        logger.info(err);
      });
  }
};
