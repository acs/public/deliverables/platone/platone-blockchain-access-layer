const certification = require('express').Router();
const getByTransaction = require('./getByTransaction');

certification.get('/transaction/:transaction', getByTransaction);

module.exports = certification;
