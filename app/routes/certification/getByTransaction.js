const libs = require('../../libs/certification');

module.exports = (req, res) => {
  const { transaction } = req.params;
  libs.get({ transactionHash: transaction })
    .then((data) => res.json(data))
    .catch((error) => res.status(500).json(error));
};
