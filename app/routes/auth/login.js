const passport = require('passport');

module.exports = (req, res) => {
  console.log(req.body);
  passport.authenticate('local-login', (error, user, message) => {
    if (!user) { return res.status(403).json(message); }
    if (error) { return res.status(500).json(error); }

    const userObject = user.toObject();
    delete userObject.password;
    return res.status(200).json(userObject);
  })(req, res);
};
