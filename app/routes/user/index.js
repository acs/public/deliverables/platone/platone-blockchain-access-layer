const users = require('express').Router();
const authorizeAdmin = require('../auth/authorizeAdmin');
const getAll = require('./getAll');
const getAllProducers = require('./getAllProducers');
const get = require('./get');
const save = require('./save');
const create = require('./create');
const current = require('./current');
const authorized = require('./authorized');

users.get('/current', current);
users.get('/authorized/:role', authorized);
users.get('/', authorizeAdmin, getAll);
users.get('/producers', getAllProducers);
users.get('/:id', authorizeAdmin, get);
users.post('/', authorizeAdmin, save);
users.post('/create', create);

module.exports = users;
