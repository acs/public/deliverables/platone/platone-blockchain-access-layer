const { MosquittoDynsec } = require('mosquitto-dynsec');
const libs = require('../../libs/user');
const config = require('../../config/config');

const ContractUtil = require('../../services/contracts').utils();
const User = require('../../models/user');

module.exports = async (req, res) => {
  const dynsec = new MosquittoDynsec();
  const account = ContractUtil.newWallet();
  console.log(account);

  try {
    await dynsec.connect({
      username: config.mosquittoAdmin,
      password: config.mosquittoPasswordAdmin,
    });
  } catch (e) {
    console.error('Error:', e);
  }

  const newUser = new User();
  newUser.role = req.body.role;
  if (req.body.role === 'producer') {
    newUser.topic = 'platone/'.concat(req.body.username);
    await dynsec.createRole(`write${req.body.username}`);
    await dynsec.addRoleACL({
      rolename: `write${req.body.username}`,
      acltype: 'publishClientSend',
      topic: newUser.topic,
      allow: true,
    });
    await dynsec.createClient({ username: req.body.username, password: newUser.generateHash('password') });
    await dynsec.addClientRole(req.body.username, `write${req.body.username}`);
  } else {
    await dynsec.createClient({ username: req.body.username, password: newUser.generateHash('password') });
  }
  newUser.username = req.body.username;
  newUser.password = newUser.generateHash('password');
  newUser.wallet = account.address;
  newUser.account = account;

  libs.save(newUser)
    .then((data) => res.json(data))
    .catch((err) => res.status(500).json(err));
};
