const routes = require('express').Router();

const users = require('./user');
const cassandraQuery = require('../test/cassandra-query');
const balQuery = require('../test/bal-query');
const auth = require('./auth');
const certification = require('./certification');
const subscription = require('./subscription');

const prefix = '/api';

routes.use(`${prefix}/users`, users);
routes.get(`${prefix}/cassandraQuery`, cassandraQuery);
routes.get(`${prefix}/balQuery`, balQuery);
routes.use(`${prefix}`, auth);
routes.use(`${prefix}/certification`, certification);
routes.use(`${prefix}/subscription`, subscription);

module.exports = routes;
