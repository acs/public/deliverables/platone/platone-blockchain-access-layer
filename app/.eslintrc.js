module.exports = {
  root: true,
  parserOptions: {
    parser: "babel-eslint",
  },
  env: {
    browser: true,
  },
  extends: ["airbnb"],
  rules: {
    "no-underscore-dangle": "off",
    "linebreak-style": 0,
    "max-len": "off",
    quotes: [0, "double"],
  },
};
