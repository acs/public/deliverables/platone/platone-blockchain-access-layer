// SPDX-License-Identifier: MIT 
pragma solidity ^0.8.0;

contract SubscriptionContract {
    struct Subscription {
        address producerWallet;
        address consumerWallet;
        string topic;
        uint256 creationDate;
    }

    address public owner;
    Subscription[] public acceptedSubscriptions;

    event SubscriptionCreated(address producerWallet, address consumerWallet, string topic, uint256 creationDate);

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner authorized");
        _;
    }

    function createSubscription(address _producerWallet, address _consumerWallet, string memory _topic, uint256 _creationDate) external {
        Subscription memory newSubscription = Subscription(_producerWallet, _consumerWallet, _topic, _creationDate);
        acceptedSubscriptions.push(newSubscription);
        emit SubscriptionCreated(_producerWallet, _consumerWallet, _topic, _creationDate);
    }

    function getAcceptedSubscriptions(string memory topic) external view returns (Subscription[] memory) {
        uint256 count = 0;
        for (uint256 i = 0; i < acceptedSubscriptions.length; i++) {
            if (keccak256(bytes(acceptedSubscriptions[i].topic)) == keccak256(bytes(topic))) {
                count++;
            }
        }
        Subscription[] memory matchingSubscriptions = new Subscription[](count);
        uint256 index = 0;
        for (uint256 i = 0; i < acceptedSubscriptions.length; i++) {
            if (keccak256(bytes(acceptedSubscriptions[i].topic)) == keccak256(bytes(topic))) {
                matchingSubscriptions[index] = acceptedSubscriptions[i];
                index++;
            }
        }
        return matchingSubscriptions;
    }
}
