// SPDX-License-Identifier: MIT 
pragma solidity ^0.8.0;

contract PlatoneCertification {
    struct Meter {
        uint256[] times;
        bytes32[] values;
    }

    address public owner;
    mapping(address => Meter) energyBalance;

    constructor() {
        owner = msg.sender;
    }

    modifier onlyOwner() {
        require(msg.sender == owner, "Only owner authorized ");
        _;
    }

    function energySnapshot(uint256 timestamp, bytes32 hash) public {
        energyBalance[msg.sender].times.push(timestamp);
        energyBalance[msg.sender].values.push(hash);
    }

    function getEnergyAccount()
        public
        view
        returns (uint256[] memory times, bytes32[] memory values)
    {
        return (
            energyBalance[msg.sender].times,
            energyBalance[msg.sender].values
        );
    }
}
