/* eslint-disable no-console */
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const passport = require('passport');
const mongoose = require('mongoose');
const routes = require('../routes');
const platoneScheduler = require('../jobs/scheduler');
// const MqttHandler = require('../jobs/mqtt_handler');
const PmuMqttHandler = require('../jobs/pmu_mqtt_handler');

const app = express();

const mongo = process.env.DATABASE_URL || 'localhost:27017';
// const mongo = process.env.DATABASE_URL || '161.27.206.144:27017';
mongoose.Promise = global.Promise;
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
mongoose.connect(`mongodb://${mongo}/bal`);

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', () => {
  console.log(`Connection Succeeded on ${mongo}`);
});

const mqttHost = process.env.MQTT_HOST || 'mqtts://platone.eng.it:8883';

// const mqttClient = new MqttHandler(mqttHost);
// mqttClient.connect();
const pmuMqttClient = new PmuMqttHandler(mqttHost);
pmuMqttClient.connect();

app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
require('../config/passport')(passport);

// routes ==================================================
app.use('/', routes);
platoneScheduler.startScheduling();

const port = process.env.PORT || 8081;
app.listen(port);

console.log(
  `Welcome to Blockchain Access Layer Application listening on port ${port}`,
);
