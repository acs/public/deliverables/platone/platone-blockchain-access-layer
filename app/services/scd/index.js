const certification = require('./certification');
const publishing = require('./publishing');

const self = {
  certification,
  publishing,
};

module.exports = self;
