/* eslint-disable no-await-in-loop */
const logger = require('../../logger');
const energyReadingLib = require('../../libs/certification');
const userLib = require('../../libs/user');
const pmuMeterReadingLib = require('../../libs/pmuMeterReading');
const meterReadingLib = require('../../libs/meterReading');
const web3 = require('../../config/utils').init;
const readingContract = require('../contracts').platoneCertification(web3);
const config = require('../../config/config');

const readingContractAddress = readingContract.getAddress();
logger.info(`readingContractAddress ${readingContractAddress}`);

function certifyReadings(keyspace) {
  userLib.getAllProducers().then(async (producers) => {
    for (let i = 0; i < producers.length; i += 1) {
      const owner = producers[i].wallet;
      const { account } = producers[i];

      const dateTo = new Date();
      let dateFrom = new Date();
      dateFrom = new Date(dateFrom.setMinutes(dateFrom.getMinutes() - 1));
      // console.log(`dateFrom  ${dateFrom}`);
      let meterReadings = null;

      if (keyspace === config.keyspaceDdemo) {
        meterReadings = await pmuMeterReadingLib.get({ owner, datetime: { $gte: new Date(dateFrom), $lte: new Date(dateTo) } });
      } else {
        meterReadings = await meterReadingLib.get({ owner, datetime: { $gte: new Date(dateFrom), $lte: new Date(dateTo) } });
      }
      if (meterReadings.length > 0) {
        const results = { meterReadings };

        logger.info(`meterReadings for ${keyspace} ${meterReadings.length}`);
        const hash = web3.utils.soliditySha3(results);

        readingContract.connect(readingContractAddress).then(async () => {
          const myContract = readingContract.getContract().contract;

          const tx = {
            // this could be provider.addresses[0] if it exists
            from: owner,
            // target address, this could be a smart contract address
            to: readingContractAddress,
            gas: 24130,
            // this encodes the ABI of the method and the arguements
            data: myContract.methods.energySnapshot(Date.now(), hash).encodeABI(),
          };
          logger.info(`TRANSACTION ${tx}`);
          logger.info(`ACCOUNT ${account.privateKey}`);
          web3.eth.accounts.signTransaction(tx, account.privateKey).then((signedTx) => {
            const sentTx = web3.eth.sendSignedTransaction(signedTx.raw || signedTx.rawTransaction);
            sentTx.on('receipt', (receipt) => {
              // do something when receipt comes back
              logger.info(`CERTIFICATION>>>${JSON.stringify(receipt)}`);
              const certification = {
                owner,
                hashedData: hash,
                transactionHash: receipt.transactionHash,
                dataset: { dateFrom, dateTo },
              };
              energyReadingLib.save(certification);
              // energyReadingLib.save(receipt);
            });
            sentTx.on('error', (err) => {
              // do something on transaction error
              logger.info(`CERTIFICATION2>>>${JSON.stringify(err)}`);

              const certification = {
                owner,
                hashedData: hash,
                transactionHash: JSON.parse(JSON.stringify(err)).receipt.transactionHash,
                dataset: { dateFrom, dateTo },
              };
              energyReadingLib.save(certification);
              // logger.error(err);
            });
          }).catch((err) => {
            // do something when promise fails
            logger.error(err);
          });
        });
      }
    }
  }).catch((err) => {
    logger.info(err);
  });
}

module.exports = certifyReadings;
