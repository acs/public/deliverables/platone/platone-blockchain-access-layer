const moment = require('moment');

const mqtt = require('mqtt');
const config = require('../../config/config');
const logger = require('../../logger');
const userLib = require('../../libs/user');
const pmuMeterReadingLib = require('../../libs/pmuMeterReading');

const options = {
  username: config.mqttUsernameDsotp,
  password: config.mqttPasswordDsotp,
  rejectedUnauthorized: false,
};

const mqttClient = mqtt.connect(config.mqttHost, options);
const topicDsotp = config.mqttTopicDsotp;

function publishingReadings() {
  userLib.getAllProducers().then((producers) => {
    for (let i = 0; i < producers.length; i += 1) {
      const params = {};
      const owner = producers[i].wallet;

      const datetimeNow = moment();
      const dateFrom = moment(datetimeNow).subtract(1, 'minutes').utc().format();
      const dateTo = datetimeNow.utc().format();

      logger.info(`owner ${owner}`);
      logger.info(`dateFrom ${dateFrom}`);
      logger.info(`dateTo ${dateTo}`);

      params.owner = owner;
      params.dateFrom = dateFrom;
      params.dateTo = dateTo;

      const meterReadings = async () => {
        await pmuMeterReadingLib.get({ owner, dateFrom, dateTo });
      };

      logger.info(`meterReadings for ${meterReadings.length}`);
      if (meterReadings.length > 0) {
        mqttClient.publish(topicDsotp, JSON.stringify(meterReadings));
        logger.info(`Message sent to Topic>>>${topicDsotp}`);
      }
    }
  });
}

module.exports = publishingReadings;
