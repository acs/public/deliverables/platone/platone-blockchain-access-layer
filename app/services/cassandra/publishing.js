const cassandra = require('cassandra-driver');
const moment = require('moment');

const { Mapper } = cassandra.mapping;
const mqtt = require('mqtt');
const config = require('../../config/config');
const logger = require('../../logger');
const producerLib = require('../../libs/producer');

const options = {
  username: config.mqttUsernameDsotp,
  password: config.mqttPasswordDsotp,
  rejectedUnauthorized: false,
};

const mqttClient = mqtt.connect(config.mqttHost, options);
const topicDsotp = config.mqttTopicDsotp;

function publishingReadings(keyspace) {
  const cassandraClient = new cassandra.Client({
    contactPoints: [config.cassandraContactPointsIP],
    localDataCenter: config.localDataCenterCassandra,
    keyspace,
  });

  const mapper = new Mapper(cassandraClient, {
    models: {
      MeterReading: {},
    },
  });
  const meterReadingMapper = mapper.forModel('MeterReading');

  producerLib.getAll().then((producers) => {
    for (let i = 0; i < producers.length; i += 1) {
      const owner = producers[i].wallet;

      cassandraClient.connect().then(async () => {
        const datetimeNow = moment();
        const query = config.queryGetMeterReadingsByOwnerAndTimestamp;
        const dateFrom = moment(datetimeNow).subtract(1, 'minutes').utc().format();
        const dateTo = datetimeNow.utc().format();

        logger.info('Read from CASSANDRA for data publishing');
        logger.info(`owner ${owner}`);
        logger.info(`dateFrom ${dateFrom}`);
        logger.info(`dateTo ${dateTo}`);

        meterReadingMapper.getMeterReadings = meterReadingMapper.mapWithQuery(query, (meterReading) => [meterReading.owner, meterReading.dateFrom, meterReading.dateTo]);

        const meterReadings = await meterReadingMapper.getMeterReadings({ owner, dateFrom, dateTo });

        logger.info(`meterReadings for ${keyspace} ${meterReadings.length}`);
        if (meterReadings.length > 0) {
          mqttClient.publish(topicDsotp, JSON.stringify(meterReadings._rs.rows));
          logger.info(`Message sent to Topic>>>${topicDsotp}`);
        }
      }).catch(async (err2) => {
        logger.error('There was an error when connecting', err2);
        await cassandraClient.shutdown();
        throw err2;
      });
    }
  }).catch((err) => {
    logger.info(err);
  });
}

module.exports = publishingReadings;
