const cassandra = require('cassandra-driver');
const moment = require('moment');

const { Mapper } = cassandra.mapping;
const config = require('../../config/config');
const logger = require('../../logger');
const energyReadingLib = require('../../libs/certification');
const producerLib = require('../../libs/producer');
const web3 = require('../../config/utils').init;
const readingContract = require('../contracts').platoneCertification(web3);
const ContractUtil = require('../contracts').utils();

const readingContractAddress = readingContract.getAddress();

function certifyReadings(keyspace) {
  const cassandraClient = new cassandra.Client({
    contactPoints: [config.cassandraContactPointsIP],
    localDataCenter: config.localDataCenterCassandra,
    keyspace,
    socketOptions: { readTimeout: 120000 },
  });

  const mapper = new Mapper(cassandraClient, {
    models: {
      MeterReading: {},
    },
  });
  const meterReadingMapper = mapper.forModel('MeterReading');

  producerLib.getAll().then((producers) => {
    for (let i = 0; i < producers.length; i += 1) {
      const owner = producers[i].wallet;
      cassandraClient.connect().then(async () => {
        const query = config.queryGetMeterReadingsByOwnerAndTimestamp;
        const dateFrom = moment().subtract(1, 'minutes').utc().format();
        const dateTo = moment().utc().format();
        const queryOptions = { fetchSize: 50000 };
        meterReadingMapper.getMeterReadings = meterReadingMapper.mapWithQuery(query, (meterReading) => [meterReading.owner, meterReading.dateFrom, meterReading.dateTo], queryOptions);

        const meterReadings = await meterReadingMapper.getMeterReadings({ owner, dateFrom, dateTo });
        if (meterReadings.length > 0) {
          logger.info(`meterReadings for ${keyspace} ${meterReadings.length}`);
          const hash = web3.utils.soliditySha3(meterReadings);
          await ContractUtil.unlock(owner);
          readingContract.connect(readingContractAddress).then(() => {
            readingContract.energySnapshot(owner, Date.now(), hash).then((data) => {
              const certification = {
                owner,
                hashedData: hash,
                transactionHash: data.tx,
                dataset: { dateFrom, dateTo },
              };
              logger.info(`CERTIFICATION>>>${JSON.stringify(certification)}`);
              energyReadingLib.save(certification);
            }).catch((err) => {
              logger.error(err);
            });
          });
        }
      }).catch(async (err2) => {
        logger.error('There was an error when connecting', err2);
        await cassandraClient.shutdown();
        throw err2;
      });
    }
  }).catch((err) => {
    logger.info(err);
  });
}

module.exports = certifyReadings;
