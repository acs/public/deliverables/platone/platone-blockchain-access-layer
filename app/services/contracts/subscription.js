/* eslint-disable no-restricted-syntax */
/* eslint-disable guard-for-in */
/* eslint-disable no-console */
/* eslint-disable no-shadow */
const truffleContract = require("@truffle/contract");
const artifact = require("../../build/contracts/SubscriptionContract.json");
const { web3ProviderFix } = require("../../config/utils");

const subscriptionContract = truffleContract(artifact);

function subscriptionCertification(web3) {
  subscriptionContract.setProvider(web3.currentProvider);
  web3ProviderFix(subscriptionContract);
  let contract;

  const deploy = async (address) => {
    const gasPrice = await web3.eth.getGasPrice();
    contract = await subscriptionContract.new({
      from: address,
      gas: 4712388,
      gasPrice,
    });
  };

  const connect = async (address) => {
    contract = await subscriptionContract.at(address);
  };

  const getAcceptedSubscriptions = async (topic) => subscriptionCertification.getAcceptedSubscriptions(topic);

  const createSubscription = async (
    producerWallet,
    consumerWallet,
    topic,
    creationDate,
  ) => {
    const contractInstance = await subscriptionContract.deployed();

    await contractInstance.createSubscription(
      producerWallet,
      consumerWallet,
      topic,
      creationDate,
    );
    console.log("Subscription created!");
  };

  const getAddress = () => {
    const { networks } = subscriptionContract;
    const addresses = [];
    for (const i in networks) {
      console.log(networks[i]);
      addresses.push(networks[i].address);
    }
    return addresses[addresses.length - 1];
  };

  const getContract = () => contract;

  return {
    connect,
    deploy,
    createSubscription,
    getAcceptedSubscriptions,
    getAddress,
    getContract,
  };
}

module.exports = subscriptionCertification;
