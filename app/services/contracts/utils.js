const web3 = require('../../config/utils').init;

function utils() {
  const newWallet = () => web3.eth.accounts.create();

  const createWallet = () => web3.eth.personal.newAccount('password');

  const unlock = (address) => web3.eth.personal.unlockAccount(address, 'password', 6000);

  const transfer = (owner, address) => web3.eth.personal.sendTransaction({ from: owner, to: address, value: web3.toWei(1, 'ether') });

  return {
    createWallet,
    unlock,
    transfer,
    newWallet,
  };
}

module.exports = utils;
