const MeterReading = require('../../models/MeterReading');

/**
 * Get a specific PmuMeterReading by id
 * @memberOf module:PmuMeterReading
 * @function get
 * @param {String} query -
 *
 * @returns {Promise} - Returns a JSON with the specific PmuMeterReading
 */

module.exports = (query) => new Promise((resolve, reject) => {
  MeterReading.find(query, (err, meterReadings) => {
    if (err) {
      reject(err);
    } else {
      resolve(meterReadings);
    }
  });
});
