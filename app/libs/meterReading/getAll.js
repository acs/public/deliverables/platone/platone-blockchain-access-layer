const MeterReading = require('../../models/MeterReading');

/**
 * Get all MeterReading
 * @memberOf module:MeterReading
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all MeterReading
 */

module.exports = () => new Promise((resolve, reject) => {
  MeterReading.find({}, (err, meterReadings) => {
    if (err) {
      reject(err);
    } else {
      resolve(meterReadings);
    }
  });
});
