const MeterReading = require('../../models/MeterReading');

/**
 * Save an MeterReading object.
 * @memberOf module:MeterReading
 * @function save
 * @param {Object} params - MeterReading params
 *
 * @returns {Promise} - Returns a JSON with the saved MeterReading
 */

module.exports = (params) => {
  const meterReadingData = new MeterReading(params);

  return new Promise((resolve, reject) => {
    MeterReading.findOneAndUpdate({ _id: meterReadingData._id },
      meterReadingData,
      { upsert: true, new: false },
      (err, meterReading) => {
        if (err) {
          reject(err);
        } else {
          resolve(meterReading);
        }
      });
  });
};
