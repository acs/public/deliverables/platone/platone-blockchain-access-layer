const PmuMeterReading = require('../../models/PMUMeterReading');

/**
 * Get a specific PmuMeterReading by id
 * @memberOf module:PmuMeterReading
 * @function get
 * @param {String} query -
 *
 * @returns {Promise} - Returns a JSON with the specific PmuMeterReading
 */

module.exports = (query) => new Promise((resolve, reject) => {
  // console.log(`query  ${JSON.stringify(query)}`);
  PmuMeterReading.find(query, (err, pmuMeterReadings) => {
    if (err) {
      reject(err);
    } else {
      resolve(pmuMeterReadings);
    }
  });
});
