const PmuMeterReading = require('../../models/PMUMeterReading');

/**
 * Get all PmuMeterReading
 * @memberOf module:PmuMeterReading
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all PmuMeterReading
 */

module.exports = () => new Promise((resolve, reject) => {
  PmuMeterReading.find({}, (err, pmuMeterReadings) => {
    if (err) {
      reject(err);
    } else {
      resolve(pmuMeterReadings);
    }
  });
});
