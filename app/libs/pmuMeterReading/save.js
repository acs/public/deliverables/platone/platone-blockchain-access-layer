const PmuMeterReading = require('../../models/PMUMeterReading');

/**
 * Save an PmuMeterReading object.
 * @memberOf module:PmuMeterReading
 * @function save
 * @param {Object} params - User params
 *
 * @returns {Promise} - Returns a JSON with the saved PmuMeterReading
 */

module.exports = (params) => {
  const pmuMeterReadingData = new PmuMeterReading(params);

  return new Promise((resolve, reject) => {
    PmuMeterReading.findOneAndUpdate({ _id: pmuMeterReadingData._id },
      pmuMeterReadingData,
      { upsert: true, new: false },
      (err, pmuMeterReading) => {
        if (err) {
          reject(err);
        } else {
          resolve(pmuMeterReading);
        }
      });
  });
};
