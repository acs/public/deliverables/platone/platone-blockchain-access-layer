const Producer = require('../../models/producer');

/**
 * Save an producer object.
 * @memberOf module:producer
 * @function save
 * @param {Object} params - User params
 *
 * @returns {Promise} - Returns a JSON with the saved producer
 */

module.exports = (params) => {
  const producerData = new Producer(params);

  return new Promise((resolve, reject) => {
    Producer.findOneAndUpdate({ _id: producerData._id },
      producerData,
      { upsert: true, new: false },
      (err, producer) => {
        if (err) {
          reject(err);
        } else {
          resolve(producer);
        }
      });
  });
};
