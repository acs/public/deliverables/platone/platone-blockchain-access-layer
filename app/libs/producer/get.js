const Producer = require('../../models/producer');

/**
 * Get a specific Producer by id
 * @memberOf module:Producer
 * @function get
 * @param {String} id - a valid Producer id
 *
 * @returns {Promise} - Returns a JSON with the specific Producer
 */

module.exports = (id) => new Promise((resolve, reject) => {
  Producer.find({ _id: id }, (err, wallets) => {
    if (err) {
      reject(err);
    } else {
      resolve(wallets);
    }
  });
});
