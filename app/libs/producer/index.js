const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const getOwnerByTopic = require('./getOwnerByTopic');

const self = {
  get,
  save,
  getAll,
  getOwnerByTopic,
};

module.exports = self;
