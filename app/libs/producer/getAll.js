const Producer = require('../../models/producer');

/**
 * Get all Producer
 * @memberOf module:Producer
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all Producer
 */

module.exports = () => new Promise((resolve, reject) => {
  Producer.find({}).populate({ path: 'owner' }).exec((err, wallets) => {
    if (err) {
      reject(err);
    } else {
      resolve(wallets);
    }
  });
});
