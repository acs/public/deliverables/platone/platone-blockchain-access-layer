const Producer = require('../../models/producer');

/**
 * Get a specific producer by topic
 * @memberOf module:Producer
 * @function get
 * @param {String} topic - a valid producer topic
 *
 * @returns {Promise} - Returns a JSON with the specific producer
 */

module.exports = (topic) => new Promise((resolve, reject) => {
  Producer.findOne({ topic }, (err, producer) => {
    if (err) {
      reject(err);
    } else if (producer) {
      resolve(producer.wallet);
    }
  });
});
