const User = require('../../models/user');

/**
 * Get a account user by username
 * @memberOf module:User
 * @function get
 * @param {String} username - a valid user username
 *
 * @returns {Promise} - Returns a JSON with the specific account
 */

module.exports = (username) => new Promise((resolve, reject) => {
  User.findOne({ username }, (err, user) => {
    if (err) {
      reject(err);
    } else if (user) {
      resolve(user.account);
    }
  });
});
