/**
 * @module User
 */

const get = require('./get');
const save = require('./save');
const getAll = require('./getAll');
const getAllProducers = require('./getAllProducers');
const getAllConsumers = require('./getAllConsumers');
const getOwnerByTopic = require('./getOwnerByTopic');
const getAccountByUsername = require("./getAccountByUsername");

const self = {
  get,
  save,
  getAll,
  getAllProducers,
  getAllConsumers,
  getOwnerByTopic,
  getAccountByUsername,
};

module.exports = self;
