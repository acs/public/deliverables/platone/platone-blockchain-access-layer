const User = require('../../models/user');

/**
 * Get a specific user by topic
 * @memberOf module:User
 * @function get
 * @param {String} topic - a valid user topic
 *
 * @returns {Promise} - Returns a JSON with the specific user
 */

module.exports = (topic, role) => new Promise((resolve, reject) => {
  User.findOne({ topic, role }, (err, user) => {
    if (err) {
      reject(err);
    } else if (user) {
      resolve(user.wallet);
    }
  });
});
