const Certification = require('../../models/certification');

/**
 * Get all user
 * @memberOf module:Certification
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all certifications
 */

module.exports = () => new Promise((resolve, reject) => {
  Certification.find({}).populate({ path: 'wallet', populate: { path: 'owner' } }).exec((err, certifications) => {
    if (err) {
      reject(err);
    } else {
      resolve(certifications);
    }
  });
});
