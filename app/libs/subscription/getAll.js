const Subscription = require('../../models/subscription');

/**
 * Get all Subscription
 * @memberOf module:Subscription
 * @function getAll
 *
 * @returns {Promise} - Returns a JSON with all Subscription
 */

module.exports = () => new Promise((resolve, reject) => {
  Subscription.find({}, (err, subscriptions) => {
    if (err) {
      reject(err);
    } else {
      resolve(subscriptions);
    }
  });
});
