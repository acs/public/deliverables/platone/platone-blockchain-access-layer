const Subscription = require('../../models/subscription');

/**
 * Save an Subscription object.
 * @memberOf module:Subscription
 * @function save
 * @param {Object} params - Subscription params
 *
 * @returns {Promise} - Returns a JSON with the saved Subscription
 */

module.exports = (params) => {
  const subscriptionData = params;
  return new Promise((resolve, reject) => {
    Subscription.findOneAndUpdate({ id_consumer: subscriptionData.id_consumer, topic: subscriptionData.topic },
      subscriptionData,
      { upsert: true, new: false },
      (err, subscription) => {
        if (err) {
          console.log(err);
          reject(err);
        } else if (!subscription) {
          resolve('Subscription not found');
        } else {
          console.log('FATTO');
          resolve(subscription);
        }
      });
  });
};
