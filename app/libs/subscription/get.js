const Subscription = require('../../models/subscription');

/**
 * Get a specific Subscription by id
 * @memberOf module:Subscription
 * @function get
 * @param {String} query -
 *
 * @returns {Promise} - Returns a JSON with the specific Subscription
 */

module.exports = (query) => new Promise((resolve, reject) => {
  Subscription.find(query, (err, subscriptions) => {
    if (err) {
      reject(err);
    } else {
      resolve(subscriptions);
    }
  });
});
