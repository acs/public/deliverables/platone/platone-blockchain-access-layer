const Web3 = require('web3');
const truffleConfig = require('../truffle-config');

const web3ProviderFix = (LoggableCounter) => {
  const newLoggableCounter = LoggableCounter;
  if (typeof newLoggableCounter.currentProvider.sendAsync !== 'function') {
    newLoggableCounter.currentProvider.sendAsync = function () {
      return newLoggableCounter.currentProvider.send.apply(
        LoggableCounter.currentProvider,
        // eslint-disable-next-line prefer-rest-params
        arguments,
      );
    };
  }
};

function init() {
  const network = process.env.CHAIN_NETWORK || 'psm';
  const { host } = truffleConfig.networks[network];
  const { port } = truffleConfig.networks[network];
  const url = `http://${host}:${port}`;

  return new Web3(new Web3.providers.HttpProvider(url));
}

module.exports = {
  init: init(),
  web3ProviderFix,
};
