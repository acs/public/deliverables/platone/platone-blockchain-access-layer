/* eslint-disable no-undef */
const PlatoneCertification = artifacts.require('./PlatoneCertification.sol');
const SubscriptionContract = artifacts.require("./SubscriptionContract.sol");

module.exports = async (deployer) => {
  await deployer.deploy(PlatoneCertification, { gas: 5047000, gasPrice: 1 });
  await deployer.deploy(SubscriptionContract, { gas: 5047000, gasPrice: 1 });
};
