const { createLogger, format, transports } = require('winston');

const {
  combine, splat, printf,
} = format;

const myFormat = printf(({
  level, message, timestamp, ...metadata
}) => {
  let msg = `${timestamp} [${level}] : ${message} `;
  if (metadata && JSON.stringify(metadata) !== '{}') {
    msg += JSON.stringify(metadata);
  }
  return msg;
});

const logger = createLogger({
  level: 'debug',
  format: combine(
    format.colorize(),
    splat(),
    format.timestamp(),
    myFormat,
  ),
  transports: [
    new transports.Console({ level: 'info' }),
    new transports.File({ filename: 'bal.log', level: 'debug' }),
  ],
});
module.exports = logger;
