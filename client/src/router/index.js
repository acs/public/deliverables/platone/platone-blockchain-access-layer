import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Signup from '@/components/Signup'
import Home from '@/components/Home'
import Verification from '@/components/Verification'
import Producer from '@/components/Producer'
import Consumer from '@/components/Consumer'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/home',
      name: 'Home',
      component: Home,
      meta: {
        requiresSession: true

      }
    },
    {
      path: '/verification',
      name: 'Verification',
      component: Verification,
      meta: {
        requiresSession: true

      }
    },
    {
      path: '/producer',
      name: 'Producer',
      component: Producer,
      meta: {
        requiresSession: true

      }
    },
    {
      path: '/consumer',
      name: 'Consumer',
      component: Consumer,
      meta: {
        requiresSession: true

      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  const reqSession = to.matched.some(route => route.meta.requiresSession)
  const reqAdmin = to.matched.some(route => route.meta.requiresAdmin)

  if (!reqSession) {
    next()
  } else if (router.app.$session.exists() && !reqAdmin) {
    next()
  } else if (reqAdmin && router.app.$session.get('user').role === 'admin') {
    next()
  } else if (reqAdmin && router.app.$session.get('user').role !== 'admin') {
    next({ name: 'Home' })
  } else {
    next({ name: 'Login' })
  }
})

export default router
