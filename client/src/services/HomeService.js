import Api from '@/services/Api'

export default {
  getCertification (params) {
    return Api().get('certification/transaction/' + params)
  },
  cassandraQuery (params) {
    return Api().get('cassandraQuery', { params })
  },
  balQuery (params) {
    return Api().get('balQuery', { params })
  }
}
