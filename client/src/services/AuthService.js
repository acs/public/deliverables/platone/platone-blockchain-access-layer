import Api from '@/services/Api'

export default {
  login (params) {
    return Api().post('login', params)
  },
  signup (params) {
    return Api().post('signup', params)
  },
  logout () {
    return Api().post('logout')
  }
}
