import Api from '@/services/Api'

export default {
  getSubscriptions (params) {
    return Api().get('subscription/' + params)
  }
}
