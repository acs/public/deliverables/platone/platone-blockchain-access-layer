import Api from '@/services/Api'

export default {
  getProducers () {
    return Api().get('users/producers/')
  },
  subscribe (params) {
    return Api().post('subscription/', params)
  }
}
