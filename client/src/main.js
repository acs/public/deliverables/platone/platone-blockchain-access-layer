// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'
import App from './App'
import router from './router'
import moment from 'moment'
import VueSession from 'vue-session'
import VueJsonPretty from 'vue-json-pretty'
import 'vue-json-pretty/lib/styles.css'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.component('v-icon', Icon)
Vue.use(VueSession)
Vue.component('vue-json-pretty', VueJsonPretty)
// You need a specific loader for CSS files

Vue.filter('formatMicroseconds', function (value) {
  console.log(value)
  if (value) {
    const stringValue = String(value).slice(0, -6)
    console.log(stringValue)
    return moment.unix(stringValue).format('DD/MM/YYYY HH:mm')
  }
})

Vue.filter('formatDate', function (value) {
  if (value) {
    return moment(String(value)).format('DD/MM/YYYY HH:mm')
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
