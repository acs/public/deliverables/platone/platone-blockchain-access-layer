# Blockchain Access Layer
## License
MIT

## Deployment
Network creation
``` bash
$ docker network create bap-net
```

SCD Container
``` bash
$ docker run --network bap-net -d --name db -v <your-volume-path>:/data/db -p 27017:27017 mongo:latest
```

Mosquitto Container
``` bash
$ cd app/mosquitto
$ docker-compose up -d
```

BAP Container
``` bash
$ cd app #location of DockerFile
$ docker build -t platone-bap:1.0 .
$ docker run --name bap -p 8082:8082 --network bap-net -e DATABASE_URL=db:27017 -e MQTT_HOST=mqtt://mosquitto_container:1883 -d platone-bap:1.0
$ docker exec -it bap bash
$ npm run deploy-local
$ npm run init-producers
$ exit
$ docker restart bap

```

## License

This project is licensed to the MIT License.

```
Copyright (c) 2021 Engineering Ingegneria Informatica S.p.a.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```

## Funding

<a rel="funding" href="https://cordis.europa.eu/project/id/864300"><img alt="PLATONE" style="border-width:0" src="client/static/platone_logo_readme.png" height="63"/></a><a rel="funding" href="https://cordis.europa.eu/project/id/864300"><img alt="H2020" style="border-width:0" src="client/static/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Platone" href="https://platone-h2020.eu/">PLATform for Operation of distribution NEtworks </a> (Platone), funded by the European Union's Horizon 2020 research and innovation programme under respectively <a rel="H2020" href="https://cordis.europa.eu/project/id/864300"> grant agreement No. 864300</a>.

## Contact

[![Engineering Logo](client/static/corporate-dark-compact.png)](https://www.eng.it)

- Vincenzo Croce <vincenzo.croce@eng.it>
- Ferdinando Bosco <ferdinando.bosco@eng.it>

